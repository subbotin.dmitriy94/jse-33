package com.tsconsulting.dsubbotin.tm.command.data.base64;

import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataBase64SaveCommand extends AbstractCommand {

    @Override
    public @NotNull String name() {
        return "data-save-base64";
    }

    @Override
    public @NotNull String description() {
        return "Save base64 data.";
    }

    @Override
    public void execute() {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        endpointLocator.getAdminEndpoint().saveBase64(session);
        TerminalUtil.printMessage("Save to base64 completed.");
    }

}

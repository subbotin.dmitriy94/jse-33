package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.endpoint.Status;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public final class ProjectUpdateStatusByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-update-status-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Update status project by id.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        endpointLocator.getProjectEndpoint().findByIdProject(session, id);
        TerminalUtil.printMessage("Enter status:");
        TerminalUtil.printMessage(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = EnumerationUtil.parseStatus(statusValue);
        endpointLocator.getProjectEndpoint().updateStatusByIdProject(session, id, status);
        TerminalUtil.printMessage("[Updated project status]");
    }

}

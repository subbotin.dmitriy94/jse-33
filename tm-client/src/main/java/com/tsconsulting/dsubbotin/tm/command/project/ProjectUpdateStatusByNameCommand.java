package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.endpoint.Status;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public final class ProjectUpdateStatusByNameCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-update-status-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Update status project by name.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        endpointLocator.getProjectEndpoint().findByNameProject(session, name);
        TerminalUtil.printMessage("Enter status:");
        TerminalUtil.printMessage(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = EnumerationUtil.parseStatus(statusValue);
        endpointLocator.getProjectEndpoint().updateStatusByNameProject(session, name, status);
        TerminalUtil.printMessage("[Updated project status]");
    }

}

package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-bind-to-project";
    }

    @Override
    @NotNull
    public String description() {
        return "Bind task to project.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter project id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        endpointLocator.getProjectEndpoint().findByIdProject(session, projectId);
        TerminalUtil.printMessage("Enter task id:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint().findByIdTask(session, taskId);
        endpointLocator.getProjectTaskEndpoint().bindTaskToProject(session, projectId, taskId);
        TerminalUtil.printMessage("[Task tied to project]");
    }

}

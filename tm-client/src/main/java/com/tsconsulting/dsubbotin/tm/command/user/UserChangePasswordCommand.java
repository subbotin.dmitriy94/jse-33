package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-change-password";
    }

    @Override
    @NotNull
    public String description() {
        return "User change password.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter new password:");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        endpointLocator.getUserEndpoint().setPasswordUser(session, newPassword);
        TerminalUtil.printMessage("Password changed.");
    }

}

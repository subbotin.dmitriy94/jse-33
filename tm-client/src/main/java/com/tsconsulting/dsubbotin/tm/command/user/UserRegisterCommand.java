package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class UserRegisterCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-register";
    }

    @Override
    @NotNull
    public String description() {
        return "User register.";
    }

    @Override
    public void execute() throws Exception {
        TerminalUtil.printMessage("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter password:");
        @NotNull final String password = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter email:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final Session session = endpointLocator.getSessionEndpoint().register(login, password, email);
        endpointLocator.getSessionService().setSession(session);
        TerminalUtil.printMessage("User registered.");
        TerminalUtil.printMessage("Logged in.");
    }

}

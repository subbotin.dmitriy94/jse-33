package com.tsconsulting.dsubbotin.tm.command.user.admin;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.AbstractException_Exception;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.endpoint.User;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class UserListShowCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-list";
    }

    @Override
    @NotNull
    public String description() {
        return "Display user list.";
    }

    @Override
    public void execute() throws AbstractException_Exception {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        final List<User> users = endpointLocator
                .getAdminUserEndpoint()
                .findAllUser(session);
        int index = 1;
        for (@NotNull final User user : users) TerminalUtil.printMessage(index++ + ". " + showUserLine(user));
    }

}

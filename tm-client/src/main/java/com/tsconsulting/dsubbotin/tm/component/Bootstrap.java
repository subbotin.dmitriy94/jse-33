package com.tsconsulting.dsubbotin.tm.component;

import com.tsconsulting.dsubbotin.tm.api.repository.ICommandRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.ISessionRepository;
import com.tsconsulting.dsubbotin.tm.api.service.*;
import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.*;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownArgumentException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownCommandException;
import com.tsconsulting.dsubbotin.tm.repository.CommandRepository;
import com.tsconsulting.dsubbotin.tm.repository.SessionRepository;
import com.tsconsulting.dsubbotin.tm.service.CommandService;
import com.tsconsulting.dsubbotin.tm.service.LogService;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.service.SessionService;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.SystemUtil;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Getter
public final class Bootstrap implements IEndpointLocator {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final ProjectTaskEndpointService projectTaskEndpointService = new ProjectTaskEndpointService();

    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint = projectTaskEndpointService.getProjectTaskEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    public void run(@Nullable final String[] args) {
        try {
            initPID();
            initCommands();
            initBackup();
            initFileScanner();
            TerminalUtil.printMessage("** WELCOME TO TASK MANAGER **");
            parseArgs(args);
            process();
        } catch (Exception e) {
            logService.error(e);
            System.exit(1);
        }
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("com.tsconsulting.dsubbotin.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(com.tsconsulting.dsubbotin.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            if (!Modifier.isAbstract(clazz.getModifiers())) registry(clazz.getConstructor().newInstance());
        }
    }

    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
        } catch (IOException e) {
            logService.error(e);
        }
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initBackup() {
        backup.start();
    }

    private void initFileScanner() {
        fileScanner.start();
    }

    private void parseArgs(@Nullable final String[] args)
            throws AbstractException {
        if (args == null || args.length == 0) return;
        @Nullable AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new UnknownArgumentException();
        try {
            command.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        command = commandService.getCommandByName("exit");
        if (command == null) throw new UnknownArgumentException();
        try {
            command.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void process() throws AbstractException {
        logService.debug("Test environment!");
        @NotNull String command = "";
        while (!isExitCommand(command)) {
            command = TerminalUtil.nextLine();
            logService.command(command);
            runCommand(command);
            logService.info("Commands '" + command + "' executed!");
        }
    }

    public void runCommand(@NotNull final String command) {
        try {
            if (EmptyUtil.isEmpty(command)) throw new UnknownCommandException();
            @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
            if (abstractCommand == null) throw new UnknownCommandException();
            abstractCommand.execute();
        } catch (Exception e) {
            logService.error(e);
        }
    }

    private void registry(@Nullable final AbstractCommand command) {
        try {
            if (command == null) throw new UnknownCommandException();
            command.setEndpointLocator(this);
            commandService.add(command);
        } catch (Exception e) {
            logService.error(e);
        }
    }

    private boolean isExitCommand(@Nullable final String command) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand;
        abstractCommand = commandService.getCommandByName("exit");
        if (abstractCommand == null) return false;
        return abstractCommand.name().equals(command);
    }

}

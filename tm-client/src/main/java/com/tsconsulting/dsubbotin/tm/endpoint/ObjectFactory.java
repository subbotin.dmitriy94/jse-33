
package com.tsconsulting.dsubbotin.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.tsconsulting.dsubbotin.tm.endpoint package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DataLoadJsonFasterXml_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "dataLoadJsonFasterXml");
    private final static QName _DataLoadJsonFasterXmlResponse_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "dataLoadJsonFasterXmlResponse");
    private final static QName _DataLoadJsonJaxb_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "dataLoadJsonJaxb");
    private final static QName _DataLoadJsonJaxbResponse_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "dataLoadJsonJaxbResponse");
    private final static QName _DataLoadXmlFasterXml_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "dataLoadXmlFasterXml");
    private final static QName _DataLoadXmlFasterXmlResponse_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "dataLoadXmlFasterXmlResponse");
    private final static QName _DataLoadXmlJaxb_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "dataLoadXmlJaxb");
    private final static QName _DataLoadXmlJaxbResponse_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "dataLoadXmlJaxbResponse");
    private final static QName _DataSaveJsonFasterXml_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "dataSaveJsonFasterXml");
    private final static QName _DataSaveJsonFasterXmlResponse_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "dataSaveJsonFasterXmlResponse");
    private final static QName _DataSaveJsonJaxb_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "dataSaveJsonJaxb");
    private final static QName _DataSaveJsonJaxbResponse_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "dataSaveJsonJaxbResponse");
    private final static QName _DataSaveXmlFasterXml_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "dataSaveXmlFasterXml");
    private final static QName _DataSaveXmlFasterXmlResponse_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "dataSaveXmlFasterXmlResponse");
    private final static QName _DataSaveXmlJaxb_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "dataSaveXmlJaxb");
    private final static QName _DataSaveXmlJaxbResponse_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "dataSaveXmlJaxbResponse");
    private final static QName _LoadBackup_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "loadBackup");
    private final static QName _LoadBackupResponse_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "loadBackupResponse");
    private final static QName _LoadBase64_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "loadBase64");
    private final static QName _LoadBase64Response_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "loadBase64Response");
    private final static QName _LoadBinary_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "loadBinary");
    private final static QName _LoadBinaryResponse_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "loadBinaryResponse");
    private final static QName _SaveBackup_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "saveBackup");
    private final static QName _SaveBackupResponse_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "saveBackupResponse");
    private final static QName _SaveBase64_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "saveBase64");
    private final static QName _SaveBase64Response_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "saveBase64Response");
    private final static QName _SaveBinary_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "saveBinary");
    private final static QName _SaveBinaryResponse_QNAME = new QName("http://endpoint.tm.dsubbotin.tsconsulting.com/", "saveBinaryResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.tsconsulting.dsubbotin.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataLoadJsonFasterXml }
     * 
     */
    public DataLoadJsonFasterXml createDataLoadJsonFasterXml() {
        return new DataLoadJsonFasterXml();
    }

    /**
     * Create an instance of {@link DataLoadJsonFasterXmlResponse }
     * 
     */
    public DataLoadJsonFasterXmlResponse createDataLoadJsonFasterXmlResponse() {
        return new DataLoadJsonFasterXmlResponse();
    }

    /**
     * Create an instance of {@link DataLoadJsonJaxb }
     * 
     */
    public DataLoadJsonJaxb createDataLoadJsonJaxb() {
        return new DataLoadJsonJaxb();
    }

    /**
     * Create an instance of {@link DataLoadJsonJaxbResponse }
     * 
     */
    public DataLoadJsonJaxbResponse createDataLoadJsonJaxbResponse() {
        return new DataLoadJsonJaxbResponse();
    }

    /**
     * Create an instance of {@link DataLoadXmlFasterXml }
     * 
     */
    public DataLoadXmlFasterXml createDataLoadXmlFasterXml() {
        return new DataLoadXmlFasterXml();
    }

    /**
     * Create an instance of {@link DataLoadXmlFasterXmlResponse }
     * 
     */
    public DataLoadXmlFasterXmlResponse createDataLoadXmlFasterXmlResponse() {
        return new DataLoadXmlFasterXmlResponse();
    }

    /**
     * Create an instance of {@link DataLoadXmlJaxb }
     * 
     */
    public DataLoadXmlJaxb createDataLoadXmlJaxb() {
        return new DataLoadXmlJaxb();
    }

    /**
     * Create an instance of {@link DataLoadXmlJaxbResponse }
     * 
     */
    public DataLoadXmlJaxbResponse createDataLoadXmlJaxbResponse() {
        return new DataLoadXmlJaxbResponse();
    }

    /**
     * Create an instance of {@link DataSaveJsonFasterXml }
     * 
     */
    public DataSaveJsonFasterXml createDataSaveJsonFasterXml() {
        return new DataSaveJsonFasterXml();
    }

    /**
     * Create an instance of {@link DataSaveJsonFasterXmlResponse }
     * 
     */
    public DataSaveJsonFasterXmlResponse createDataSaveJsonFasterXmlResponse() {
        return new DataSaveJsonFasterXmlResponse();
    }

    /**
     * Create an instance of {@link DataSaveJsonJaxb }
     * 
     */
    public DataSaveJsonJaxb createDataSaveJsonJaxb() {
        return new DataSaveJsonJaxb();
    }

    /**
     * Create an instance of {@link DataSaveJsonJaxbResponse }
     * 
     */
    public DataSaveJsonJaxbResponse createDataSaveJsonJaxbResponse() {
        return new DataSaveJsonJaxbResponse();
    }

    /**
     * Create an instance of {@link DataSaveXmlFasterXml }
     * 
     */
    public DataSaveXmlFasterXml createDataSaveXmlFasterXml() {
        return new DataSaveXmlFasterXml();
    }

    /**
     * Create an instance of {@link DataSaveXmlFasterXmlResponse }
     * 
     */
    public DataSaveXmlFasterXmlResponse createDataSaveXmlFasterXmlResponse() {
        return new DataSaveXmlFasterXmlResponse();
    }

    /**
     * Create an instance of {@link DataSaveXmlJaxb }
     * 
     */
    public DataSaveXmlJaxb createDataSaveXmlJaxb() {
        return new DataSaveXmlJaxb();
    }

    /**
     * Create an instance of {@link DataSaveXmlJaxbResponse }
     * 
     */
    public DataSaveXmlJaxbResponse createDataSaveXmlJaxbResponse() {
        return new DataSaveXmlJaxbResponse();
    }

    /**
     * Create an instance of {@link LoadBackup }
     * 
     */
    public LoadBackup createLoadBackup() {
        return new LoadBackup();
    }

    /**
     * Create an instance of {@link LoadBackupResponse }
     * 
     */
    public LoadBackupResponse createLoadBackupResponse() {
        return new LoadBackupResponse();
    }

    /**
     * Create an instance of {@link LoadBase64 }
     * 
     */
    public LoadBase64 createLoadBase64() {
        return new LoadBase64();
    }

    /**
     * Create an instance of {@link LoadBase64Response }
     * 
     */
    public LoadBase64Response createLoadBase64Response() {
        return new LoadBase64Response();
    }

    /**
     * Create an instance of {@link LoadBinary }
     * 
     */
    public LoadBinary createLoadBinary() {
        return new LoadBinary();
    }

    /**
     * Create an instance of {@link LoadBinaryResponse }
     * 
     */
    public LoadBinaryResponse createLoadBinaryResponse() {
        return new LoadBinaryResponse();
    }

    /**
     * Create an instance of {@link SaveBackup }
     * 
     */
    public SaveBackup createSaveBackup() {
        return new SaveBackup();
    }

    /**
     * Create an instance of {@link SaveBackupResponse }
     * 
     */
    public SaveBackupResponse createSaveBackupResponse() {
        return new SaveBackupResponse();
    }

    /**
     * Create an instance of {@link SaveBase64 }
     * 
     */
    public SaveBase64 createSaveBase64() {
        return new SaveBase64();
    }

    /**
     * Create an instance of {@link SaveBase64Response }
     * 
     */
    public SaveBase64Response createSaveBase64Response() {
        return new SaveBase64Response();
    }

    /**
     * Create an instance of {@link SaveBinary }
     * 
     */
    public SaveBinary createSaveBinary() {
        return new SaveBinary();
    }

    /**
     * Create an instance of {@link SaveBinaryResponse }
     * 
     */
    public SaveBinaryResponse createSaveBinaryResponse() {
        return new SaveBinaryResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadJsonFasterXml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadJsonFasterXml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "dataLoadJsonFasterXml")
    public JAXBElement<DataLoadJsonFasterXml> createDataLoadJsonFasterXml(DataLoadJsonFasterXml value) {
        return new JAXBElement<DataLoadJsonFasterXml>(_DataLoadJsonFasterXml_QNAME, DataLoadJsonFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadJsonFasterXmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadJsonFasterXmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "dataLoadJsonFasterXmlResponse")
    public JAXBElement<DataLoadJsonFasterXmlResponse> createDataLoadJsonFasterXmlResponse(DataLoadJsonFasterXmlResponse value) {
        return new JAXBElement<DataLoadJsonFasterXmlResponse>(_DataLoadJsonFasterXmlResponse_QNAME, DataLoadJsonFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadJsonJaxb }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadJsonJaxb }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "dataLoadJsonJaxb")
    public JAXBElement<DataLoadJsonJaxb> createDataLoadJsonJaxb(DataLoadJsonJaxb value) {
        return new JAXBElement<DataLoadJsonJaxb>(_DataLoadJsonJaxb_QNAME, DataLoadJsonJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadJsonJaxbResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadJsonJaxbResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "dataLoadJsonJaxbResponse")
    public JAXBElement<DataLoadJsonJaxbResponse> createDataLoadJsonJaxbResponse(DataLoadJsonJaxbResponse value) {
        return new JAXBElement<DataLoadJsonJaxbResponse>(_DataLoadJsonJaxbResponse_QNAME, DataLoadJsonJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadXmlFasterXml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadXmlFasterXml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "dataLoadXmlFasterXml")
    public JAXBElement<DataLoadXmlFasterXml> createDataLoadXmlFasterXml(DataLoadXmlFasterXml value) {
        return new JAXBElement<DataLoadXmlFasterXml>(_DataLoadXmlFasterXml_QNAME, DataLoadXmlFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadXmlFasterXmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadXmlFasterXmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "dataLoadXmlFasterXmlResponse")
    public JAXBElement<DataLoadXmlFasterXmlResponse> createDataLoadXmlFasterXmlResponse(DataLoadXmlFasterXmlResponse value) {
        return new JAXBElement<DataLoadXmlFasterXmlResponse>(_DataLoadXmlFasterXmlResponse_QNAME, DataLoadXmlFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadXmlJaxb }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadXmlJaxb }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "dataLoadXmlJaxb")
    public JAXBElement<DataLoadXmlJaxb> createDataLoadXmlJaxb(DataLoadXmlJaxb value) {
        return new JAXBElement<DataLoadXmlJaxb>(_DataLoadXmlJaxb_QNAME, DataLoadXmlJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadXmlJaxbResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadXmlJaxbResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "dataLoadXmlJaxbResponse")
    public JAXBElement<DataLoadXmlJaxbResponse> createDataLoadXmlJaxbResponse(DataLoadXmlJaxbResponse value) {
        return new JAXBElement<DataLoadXmlJaxbResponse>(_DataLoadXmlJaxbResponse_QNAME, DataLoadXmlJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveJsonFasterXml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveJsonFasterXml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "dataSaveJsonFasterXml")
    public JAXBElement<DataSaveJsonFasterXml> createDataSaveJsonFasterXml(DataSaveJsonFasterXml value) {
        return new JAXBElement<DataSaveJsonFasterXml>(_DataSaveJsonFasterXml_QNAME, DataSaveJsonFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveJsonFasterXmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveJsonFasterXmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "dataSaveJsonFasterXmlResponse")
    public JAXBElement<DataSaveJsonFasterXmlResponse> createDataSaveJsonFasterXmlResponse(DataSaveJsonFasterXmlResponse value) {
        return new JAXBElement<DataSaveJsonFasterXmlResponse>(_DataSaveJsonFasterXmlResponse_QNAME, DataSaveJsonFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveJsonJaxb }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveJsonJaxb }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "dataSaveJsonJaxb")
    public JAXBElement<DataSaveJsonJaxb> createDataSaveJsonJaxb(DataSaveJsonJaxb value) {
        return new JAXBElement<DataSaveJsonJaxb>(_DataSaveJsonJaxb_QNAME, DataSaveJsonJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveJsonJaxbResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveJsonJaxbResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "dataSaveJsonJaxbResponse")
    public JAXBElement<DataSaveJsonJaxbResponse> createDataSaveJsonJaxbResponse(DataSaveJsonJaxbResponse value) {
        return new JAXBElement<DataSaveJsonJaxbResponse>(_DataSaveJsonJaxbResponse_QNAME, DataSaveJsonJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveXmlFasterXml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveXmlFasterXml }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "dataSaveXmlFasterXml")
    public JAXBElement<DataSaveXmlFasterXml> createDataSaveXmlFasterXml(DataSaveXmlFasterXml value) {
        return new JAXBElement<DataSaveXmlFasterXml>(_DataSaveXmlFasterXml_QNAME, DataSaveXmlFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveXmlFasterXmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveXmlFasterXmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "dataSaveXmlFasterXmlResponse")
    public JAXBElement<DataSaveXmlFasterXmlResponse> createDataSaveXmlFasterXmlResponse(DataSaveXmlFasterXmlResponse value) {
        return new JAXBElement<DataSaveXmlFasterXmlResponse>(_DataSaveXmlFasterXmlResponse_QNAME, DataSaveXmlFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveXmlJaxb }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveXmlJaxb }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "dataSaveXmlJaxb")
    public JAXBElement<DataSaveXmlJaxb> createDataSaveXmlJaxb(DataSaveXmlJaxb value) {
        return new JAXBElement<DataSaveXmlJaxb>(_DataSaveXmlJaxb_QNAME, DataSaveXmlJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveXmlJaxbResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveXmlJaxbResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "dataSaveXmlJaxbResponse")
    public JAXBElement<DataSaveXmlJaxbResponse> createDataSaveXmlJaxbResponse(DataSaveXmlJaxbResponse value) {
        return new JAXBElement<DataSaveXmlJaxbResponse>(_DataSaveXmlJaxbResponse_QNAME, DataSaveXmlJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBackup }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadBackup }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "loadBackup")
    public JAXBElement<LoadBackup> createLoadBackup(LoadBackup value) {
        return new JAXBElement<LoadBackup>(_LoadBackup_QNAME, LoadBackup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBackupResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadBackupResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "loadBackupResponse")
    public JAXBElement<LoadBackupResponse> createLoadBackupResponse(LoadBackupResponse value) {
        return new JAXBElement<LoadBackupResponse>(_LoadBackupResponse_QNAME, LoadBackupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBase64 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadBase64 }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "loadBase64")
    public JAXBElement<LoadBase64> createLoadBase64(LoadBase64 value) {
        return new JAXBElement<LoadBase64>(_LoadBase64_QNAME, LoadBase64 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBase64Response }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadBase64Response }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "loadBase64Response")
    public JAXBElement<LoadBase64Response> createLoadBase64Response(LoadBase64Response value) {
        return new JAXBElement<LoadBase64Response>(_LoadBase64Response_QNAME, LoadBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBinary }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadBinary }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "loadBinary")
    public JAXBElement<LoadBinary> createLoadBinary(LoadBinary value) {
        return new JAXBElement<LoadBinary>(_LoadBinary_QNAME, LoadBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBinaryResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadBinaryResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "loadBinaryResponse")
    public JAXBElement<LoadBinaryResponse> createLoadBinaryResponse(LoadBinaryResponse value) {
        return new JAXBElement<LoadBinaryResponse>(_LoadBinaryResponse_QNAME, LoadBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBackup }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveBackup }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "saveBackup")
    public JAXBElement<SaveBackup> createSaveBackup(SaveBackup value) {
        return new JAXBElement<SaveBackup>(_SaveBackup_QNAME, SaveBackup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBackupResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveBackupResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "saveBackupResponse")
    public JAXBElement<SaveBackupResponse> createSaveBackupResponse(SaveBackupResponse value) {
        return new JAXBElement<SaveBackupResponse>(_SaveBackupResponse_QNAME, SaveBackupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBase64 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveBase64 }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "saveBase64")
    public JAXBElement<SaveBase64> createSaveBase64(SaveBase64 value) {
        return new JAXBElement<SaveBase64>(_SaveBase64_QNAME, SaveBase64 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBase64Response }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveBase64Response }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "saveBase64Response")
    public JAXBElement<SaveBase64Response> createSaveBase64Response(SaveBase64Response value) {
        return new JAXBElement<SaveBase64Response>(_SaveBase64Response_QNAME, SaveBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBinary }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveBinary }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "saveBinary")
    public JAXBElement<SaveBinary> createSaveBinary(SaveBinary value) {
        return new JAXBElement<SaveBinary>(_SaveBinary_QNAME, SaveBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBinaryResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveBinaryResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.dsubbotin.tsconsulting.com/", name = "saveBinaryResponse")
    public JAXBElement<SaveBinaryResponse> createSaveBinaryResponse(SaveBinaryResponse value) {
        return new JAXBElement<SaveBinaryResponse>(_SaveBinaryResponse_QNAME, SaveBinaryResponse.class, null, value);
    }

}

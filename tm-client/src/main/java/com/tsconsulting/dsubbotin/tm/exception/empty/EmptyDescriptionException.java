package com.tsconsulting.dsubbotin.tm.exception.empty;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public final class EmptyDescriptionException extends AbstractException {

    public EmptyDescriptionException() {
        super("Empty description entered.");
    }

}

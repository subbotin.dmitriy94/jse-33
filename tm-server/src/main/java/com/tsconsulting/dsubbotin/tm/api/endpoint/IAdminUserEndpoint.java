package com.tsconsulting.dsubbotin.tm.api.endpoint;

import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Session;
import com.tsconsulting.dsubbotin.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminUserEndpoint {

    @WebMethod
    void createUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password,
            @NotNull @WebParam(name = "email") String email
    ) throws AbstractException;

    @NotNull
    @WebMethod
    List<User> findAllUser(
            @Nullable @WebParam(name = "session") Session session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    User findByIdUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @NotNull
    @WebMethod
    User findByLoginUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String login
    ) throws AbstractException;

    @NotNull
    @WebMethod
    User findByIndexUser(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException;

    @WebMethod
    void removeByLoginUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String login
    ) throws AbstractException;

    @WebMethod
    void removeByIdUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException;

    @WebMethod
    void removeByIndexUser(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException;

    @WebMethod
    void clearUser(
            @Nullable @WebParam(name = "session") Session session
    ) throws AbstractException;

    @WebMethod
    void setRoleUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id,
            @NotNull @WebParam(name = "role") Role role
    ) throws AbstractException;

    @WebMethod
    void lockByLoginUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String login
    ) throws AbstractException;

    @WebMethod
    void unlockByLoginUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String login
    ) throws AbstractException;

}

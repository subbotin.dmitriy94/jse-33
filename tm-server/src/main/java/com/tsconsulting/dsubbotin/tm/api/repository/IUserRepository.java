package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;
import org.jetbrains.annotations.NotNull;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User findByLogin(@NotNull String login) throws AbstractException;

    void removeByLogin(@NotNull String login) throws AbstractException;

    boolean isLogin(@NotNull String login);

}

package com.tsconsulting.dsubbotin.tm.component;

import com.tsconsulting.dsubbotin.tm.api.endpoint.ISessionEndpoint;
import com.tsconsulting.dsubbotin.tm.api.repository.*;
import com.tsconsulting.dsubbotin.tm.api.service.*;
import com.tsconsulting.dsubbotin.tm.endpoint.*;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.repository.*;
import com.tsconsulting.dsubbotin.tm.service.*;
import com.tsconsulting.dsubbotin.tm.util.SystemUtil;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService =
            new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, this);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    public void run(@Nullable final String[] args) {
        try {
            initPID();
            initUsers();
            TerminalUtil.printMessage("** START SERVER TASK MANAGER **");
            TerminalUtil.printMessage("[PID: " + SystemUtil.getPID() + "]");
            initEndpoints();
        } catch (Exception e) {
            logService.error(e);
            System.exit(1);
        }
    }

    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
        } catch (IOException e) {
            logService.error(e);
        }
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initEndpoints() {
        initEndpoint(projectEndpoint);
        initEndpoint(taskEndpoint);
        initEndpoint(projectTaskEndpoint);
        initEndpoint(adminEndpoint);
        initEndpoint(adminUserEndpoint);
        initEndpoint(userEndpoint);
        initEndpoint(sessionEndpoint);
    }

    private void initEndpoint(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        Endpoint.publish(wsdl, endpoint);
        TerminalUtil.printMessage(wsdl);
    }

    private void initUsers() throws AbstractException {
        userService.create("admin", "admin", Role.ADMIN);
        userService.create("test", "test", Role.USER);
        userService.create("user", "user", Role.USER);
    }

}

package com.tsconsulting.dsubbotin.tm.endpoint;

import com.tsconsulting.dsubbotin.tm.api.endpoint.IProjectEndpoint;
import com.tsconsulting.dsubbotin.tm.api.service.IServiceLocator;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.EntityNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.AccessDeniedException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.model.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint() {
        super(null);
    }

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public boolean existByIdProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AccessDeniedException, EntityNotFoundException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getProjectService().existById(userId, id);
    }

    @Override
    @WebMethod
    public void addProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") Project entity
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().add(userId, entity);
    }

    @Override
    @WebMethod
    public void removeProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") Project entity
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().remove(userId, entity);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Project> findAllProject(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "sort") String sort
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getProjectService().findAll(userId, sort);
    }

    @Override
    @NotNull
    @WebMethod
    public Project findByIdProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getProjectService().findById(userId, id);
    }

    @Override
    @NotNull
    @WebMethod
    public Project findByIndexProject(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getProjectService().findByIndex(userId, index);
    }

    @Override
    @WebMethod
    public void removeByIdProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().removeById(userId, id);
    }

    @Override
    @WebMethod
    public void removeByIndexProject(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().removeByIndex(userId, index);
    }

    @Override
    @WebMethod
    public void createProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().create(userId, name, description);
    }

    @Override
    @NotNull

    @WebMethod
    public Project findByNameProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getProjectService().findByName(userId, name);
    }

    @Override
    @WebMethod
    public void updateByIdProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id,
            @NotNull @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().updateById(userId, id, name, description);
    }

    @Override
    @WebMethod
    public void updateByIndexProject(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index,
            @NotNull @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().updateByIndex(userId, index, name, description);
    }

    @Override
    @WebMethod
    public void startByIdProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().startById(userId, id);
    }

    @Override
    @WebMethod
    public void startByIndexProject(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().startByIndex(userId, index);
    }

    @Override
    @WebMethod
    public void startByNameProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().startByName(userId, name);
    }

    @Override
    @WebMethod
    public void finishByIdProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().finishById(userId, id);
    }

    @Override
    @WebMethod
    public void finishByIndexProject(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().finishByIndex(userId, index);
    }

    @Override
    @WebMethod
    public void finishByNameProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().finishByName(userId, name);
    }

    @Override
    @WebMethod
    public void updateStatusByIdProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id,
            @NotNull @WebParam(name = "status") Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().updateStatusById(userId, id, status);
    }

    @Override
    @WebMethod
    public void updateStatusByIndexProject(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index,
            @NotNull @WebParam(name = "status") Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().updateStatusByIndex(userId, index, status);
    }

    @Override
    @WebMethod
    public void updateStatusByNameProject(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "status") Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getProjectService().updateStatusByName(userId, name, status);
    }

}

package com.tsconsulting.dsubbotin.tm.endpoint;

import com.tsconsulting.dsubbotin.tm.api.endpoint.ITaskEndpoint;
import com.tsconsulting.dsubbotin.tm.api.service.IServiceLocator;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Session;
import com.tsconsulting.dsubbotin.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService

public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint() {
        super(null);
    }

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public boolean existByIdTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getTaskService().existById(userId, id);
    }

    @Override
    @WebMethod
    public void addTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") Task entity
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().add(userId, entity);
    }

    @Override
    @WebMethod
    public void removeTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") Task entity
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().remove(userId, entity);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Task> findAllTask(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "sort") String sort
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getTaskService().findAll(userId, sort);
    }

    @Override
    @NotNull
    @WebMethod
    public Task findByIdTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getTaskService().findById(userId, id);
    }

    @Override
    @NotNull
    @WebMethod
    public Task findByIndexTask(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getTaskService().findByIndex(userId, index);
    }

    @Override
    @WebMethod
    public void removeByIdTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().removeById(userId, id);
    }

    @Override
    @WebMethod
    public void removeByIndexTask(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().removeByIndex(userId, index);
    }

    @Override
    @WebMethod
    public void createTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().create(userId, name, description);
    }

    @Override
    @WebMethod
    public void clearTask(
            @Nullable @WebParam(name = "session") Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().clear(userId);
    }

    @Override
    @NotNull
    @WebMethod
    public Task findByNameTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getTaskService().findByName(userId, name);
    }

    @Override
    @WebMethod
    public void removeByNameTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().removeByName(userId, name);
    }

    @Override
    @WebMethod
    public void updateByIdTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id,
            @NotNull @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().updateById(userId, id, name, description);
    }

    @Override
    @WebMethod
    public void updateByIndexTask(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index,
            @NotNull @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().updateByIndex(userId, index, name, description);
    }

    @Override
    @WebMethod
    public void startByIdTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().startById(userId, id);
    }

    @Override
    @WebMethod
    public void startByIndexTask(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().startByIndex(userId, index);
    }

    @Override
    @WebMethod
    public void startByNameTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().startByName(userId, name);
    }

    @Override
    @WebMethod
    public void finishByIdTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().finishById(userId, id);
    }

    @Override
    @WebMethod
    public void finishByIndexTask(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().finishByIndex(userId, index);
    }

    @Override
    @WebMethod
    public void finishByNameTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().finishByName(userId, name);
    }

    @Override
    @WebMethod
    public void updateStatusByIdTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id,
            @NotNull @WebParam(name = "status") Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().updateStatusById(userId, id, status);
    }

    @Override
    @WebMethod
    public void updateStatusByIndexTask(
            @Nullable @WebParam(name = "session") Session session,
            @WebParam(name = "index") int index,
            @NotNull @WebParam(name = "status") Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().updateStatusByIndex(userId, index, status);
    }

    @Override
    @WebMethod
    public void updateStatusByNameTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "status") Status status
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().updateStatusByName(userId, name, status);
    }

}

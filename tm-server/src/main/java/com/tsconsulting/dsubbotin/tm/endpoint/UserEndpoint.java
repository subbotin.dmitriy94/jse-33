package com.tsconsulting.dsubbotin.tm.endpoint;

import com.tsconsulting.dsubbotin.tm.api.endpoint.IUserEndpoint;
import com.tsconsulting.dsubbotin.tm.api.service.IServiceLocator;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Session;
import com.tsconsulting.dsubbotin.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService

public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint() {
        super(null);
    }

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void setPasswordUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "password") String password
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getUserService().setPassword(userId, password);
    }

    @Override
    @WebMethod
    public void updateByIdUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "lastName") String lastName,
            @NotNull @WebParam(name = "firstName") String firstName,
            @NotNull @WebParam(name = "middleName") String middleName,
            @NotNull @WebParam(name = "email") String email
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getUserService().updateById(userId, lastName, firstName, middleName, email);
    }

    @Override
    @WebMethod
    public boolean isLoginUser(
            @NotNull @WebParam(name = "login") String login
    ) throws AbstractException {
        return serviceLocator.getUserService().isLogin(login);
    }

    @Override
    public User getUser(
            @Nullable @WebParam(name = "session") Session session
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getUserService().findById(userId);
    }

}

package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IRepository;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.EntityNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @Override
    public void add(@NotNull final E entity) {
        entities.add(entity);
    }

    public void addAll(@NotNull final List<E> entitiesList) {
        entities.addAll(entitiesList);
    }

    @Override
    public void remove(@NotNull final E entity) {
        entities.remove(entity);
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    @NotNull
    public List<E> findAll() {
        return entities;
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        @NotNull final List<E> entitiesList = new ArrayList<>(entities);
        entitiesList.sort(comparator);
        return entitiesList;
    }

    @Override
    @NotNull
    public E findById(@NotNull final String id) throws AbstractException {
        return entities.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @NotNull
    public E findByIndex(final int index) throws AbstractException {
        if (entities.size() - 1 < index) throw new IndexIncorrectException();
        return entities.get(index);
    }

    @Override
    public void removeById(@NotNull final String id) throws AbstractException {
        @NotNull final E entity = findById(id);
        remove(entity);
    }

    @Override
    public void removeByIndex(final int index) throws AbstractException {
        @NotNull final E entity = findByIndex(index);
        entities.remove(entity);
    }

}
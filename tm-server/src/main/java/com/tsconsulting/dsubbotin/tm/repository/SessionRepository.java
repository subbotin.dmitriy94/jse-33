package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ISessionRepository;
import com.tsconsulting.dsubbotin.tm.model.Session;
import org.jetbrains.annotations.NotNull;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public void open(@NotNull final Session session) {
        entities.add(session);
    }

    @Override
    public boolean close(@NotNull final Session session) {
        return entities.removeIf(e -> session.getId().equals(e.getId()));
    }

    @Override
    public boolean contains(@NotNull final String id) {
        return entities.stream().anyMatch(e -> id.equals(e.getId()));
    }
}
